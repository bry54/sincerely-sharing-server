<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/mobile/test', 'Mobile\AuthController@test');

Route::post('/auth/login', 'Mobile\AuthController@login');
Route::post('/auth/logout', 'Mobile\AuthController@logout');
Route::post('/auth/register', 'Mobile\AuthController@register');

Route::post('/posts/get-all', 'Mobile\PostsController@getAllPosts');
Route::post('/posts/get-all-categories', 'Mobile\PostsController@getAllCategories');


Route::middleware('auth:api')->group(function () {
    Route::get('/mobile/user', 'PassportController@details');

    Route::post('/posts/get-posts', 'Mobile\PostsController@getPosts');
    Route::post('/posts/get-post-detail', 'Mobile\PostsController@getPostDetail');
    Route::post('/posts/follow-post', 'Mobile\PostsController@followPost');
    Route::post('/posts/submit-vote-for-post', 'Mobile\VotesController@voteForPost');

    Route::post('/posts/submit-post', 'Mobile\PostsController@submitPost');
    Route::post('/posts/delete-post', 'Mobile\PostsController@deletePost');
    Route::post('/posts/archive-post', 'Mobile\PostsController@archivePost');

    Route::post('/comments/get-comment-detail', 'Mobile\CommentsController@getCommentDetail');
    Route::post('/comments/follow-comment', 'Mobile\CommentsController@followComment');
    Route::post('/comments/submit-vote-for-comment', 'Mobile\VotesController@voteForComment');
    Route::post('/comments/submit-comment-for-post', 'Mobile\CommentsController@submitCommentForPost');
    Route::post('/comments/submit-comment-for-comment', 'Mobile\CommentsController@submitCommentForComment');
    Route::post('/comments/delete-comment', 'Mobile\CommentsController@deleteComment');
    Route::post('/comments/archive-comment', 'Mobile\CommentsController@archiveComment');

    Route::post('/communities/get-all-communities', 'Mobile\CommunitiesController@getAllCommunities');
    Route::post('/communities/get-subscribed-communities', 'Mobile\CommunitiesController@getSubscribedCommunities');
    Route::post('/communities/request-community', 'Mobile\CommunitiesController@requestCommunity');
    Route::post('/communities/vote-request-community', 'Mobile\CommunitiesController@voteRequestCommunity');
    Route::post('/communities/toggle-community-subscription', 'Mobile\CommunitiesController@toggleCommunitySubscription');

    Route::post('/user/my-profile', 'Mobile\ProfileController@getMyProfile');

    //Needs to recheck redundant routes below
    
    Route::post('/posts/get-posts-from-category', 'Mobile\PostsController@getPostsFromCategory');
    
    Route::post('/posts/edit-post', 'Mobile\PostsController@editPost');
    Route::post('/comments/edit-comment', 'Mobile\CommentsController@editComment');

    Route::post('/communities/subscribe-to-community', 'Mobile\CommunitiesController@subscribeToCommunity');
    Route::post('/communities/leave-community', 'Mobile\CommunitiesController@leaveCommunity');
    Route::post('/comments/get-comment-comments', 'Mobile\CommentsController@fetchCommentComments');
    Route::post('/comments/get-my-comments', 'Mobile\CommentsController@getMyComments');

    Route::post('/posts/get-my-posts', 'Mobile\PostsController@getMyPosts');
    Route::post('/posts/get-all-from-subscribed-communities', 'Mobile\PostsController@getPostsFromSubscribedCommunities');

    Route::post('/posts/get-post-comments', 'Mobile\PostsController@getPostComments');
    Route::post('/posts/get-posts-from-specific-community', 'Mobile\PostsController@getPostsFromSpecificCommunity');
});