<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/dashboard');
});

Route::get('dashboard', 'Web\DashboardController@index');

Route::get('communities', 'Web\CommunitiesController@index');
Route::get('communities/get-communities', 'Web\CommunitiesController@getCommunities');
Route::get('communities/topic-management', 'Web\CommunitiesController@manageTopics');
Route::get('communities/send-communities', 'Web\CommunitiesController@sendNotifications');

Route::post('communities/to-subscriber', 'Web\CommunitiesController@sendNotificationToSubscriber');
Route::post('communities/to-subscribers', 'Web\CommunitiesController@sendNotificationToTopicSubscribers');
Route::post('communities/add-topic', 'Web\CommunitiesController@addTopic');
Route::post('communities/delete-topic', 'Web\CommunitiesController@deleteTopic');

Route::get('posts', 'Web\PostsController@index');
Route::get('posts/get-posts', 'Web\PostsController@getPosts');
Route::get('communities/topic-management', 'Web\CommunitiesController@manageTopics');
Route::get('communities/send-communities', 'Web\CommunitiesController@sendNotifications');

Route::post('communities/to-subscriber', 'Web\CommunitiesController@sendNotificationToSubscriber');
Route::post('communities/to-subscribers', 'Web\CommunitiesController@sendNotificationToTopicSubscribers');
Route::post('communities/add-topic', 'Web\CommunitiesController@addTopic');
Route::post('communities/delete-topic', 'Web\CommunitiesController@deleteTopic');

Route::get('users', 'Web\UsersController@index');
Route::get('users/get-users', 'Web\UsersController@getUsers');
Route::get('communities/topic-management', 'Web\CommunitiesController@manageTopics');
Route::get('communities/send-communities', 'Web\CommunitiesController@sendNotifications');

Route::post('communities/to-subscriber', 'Web\CommunitiesController@sendNotificationToSubscriber');
Route::post('communities/to-subscribers', 'Web\CommunitiesController@sendNotificationToTopicSubscribers');
Route::post('communities/add-topic', 'Web\CommunitiesController@addTopic');
Route::post('communities/delete-topic', 'Web\CommunitiesController@deleteTopic');
