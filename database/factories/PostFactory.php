<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Post::class, function (Faker $faker) {
    $users = App\User::pluck('id')->toArray();
    $categories = App\Models\Category::pluck('id')->toArray();

    $u = $faker->randomElement($users);
    $communities = \App\User::find($u)->communities()->pluck('community_id')->toArray();
    return [
        'author_id' => $u,
        'title' => $faker->sentence(15),
        'body' => $faker->paragraph(15),
        'category_id' => $faker->randomElement($categories),
        'community_id' => $faker->randomElement($communities),
        'is_archived' => $faker->boolean(),
        'is_deleted' => $faker->boolean(),

        'confidence' => $faker->randomFloat(),
        'hotness' => $faker->randomFloat(),
    ];
});
