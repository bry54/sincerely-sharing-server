<?php

use Faker\Generator as Faker;

$factory->define(App\Models\CommentVote::class, function (Faker $faker) {
    $users = App\User::pluck('id')->toArray();
    $comments = App\Models\Comment::pluck('id')->toArray();
    $vote = $faker->boolean;

    return [
        'user_id' => $faker->randomElement($users),
        'comment_id' => $faker->randomElement($comments),
        'up_vote' => $vote,
        'down_vote' => !$vote,
    ];
});
