<?php

use Faker\Generator as Faker;

$factory->define(App\Pivots\PostUser::class, function (Faker $faker) {
    $users = App\User::pluck('id')->toArray();
    $posts = App\Models\Post::pluck('id')->toArray();

    return [
        'user_id'=>$faker->randomElement($users),
        'post_id'=>$faker->randomElement($posts),
    ];
});
