<?php

use Faker\Generator as Faker;

$factory->define(App\Models\PostVote::class, function (Faker $faker) {
    $users = App\User::pluck('id')->toArray();
    $posts = App\Models\Post::pluck('id')->toArray();
    $vote = $faker->boolean;

    return [
        'user_id' => $faker->randomElement($users),
        'post_id' => $faker->randomElement($posts),
        'up_vote' => $vote,
        'down_vote' => !$vote,
    ];
});
