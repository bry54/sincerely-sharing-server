<?php

use Faker\Generator as Faker;

$factory->define(App\Pivots\CommunityUser::class, function (Faker $faker) {
    $users = App\User::pluck('id')->toArray();

    return [
        'user_id'=>$faker->randomElement($users),
        'community_id'=>1,
    ];
});
