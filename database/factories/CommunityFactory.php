<?php

use App\Models\Community;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

$factory->define(Community::class, function (Faker $faker) {
    return [
        'name' => $faker->words(3,true),
        'summary' => $faker->paragraph(10),
        'password' => rand(0, 1) ? '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm' : null, // secret
    ];
});
