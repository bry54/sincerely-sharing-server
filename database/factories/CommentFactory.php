<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Comment::class, function (Faker $faker) {
    //'body', 'author_id', 'post_id'
    $users = App\User::pluck('id')->toArray();
    $posts = App\Models\Post::pluck('id')->toArray();
    $comments = App\Models\Comment::pluck('id')->toArray();

    return [
        'author_id' => $faker->randomElement($users),
        'body' => $faker->paragraph(15),
        'post_id' => $faker->randomElement($posts),
        'parent_id' => $faker->randomElement($comments),

        'confidence' => $faker->randomFloat(),
        'hotness' => $faker->randomFloat(),
        'is_archived' => $faker->boolean(),
        'is_deleted' => $faker->boolean(),
    ];
});
