<?php

use Faker\Generator as Faker;

$factory->define(App\Pivots\CategoryCommunity::class, function (Faker $faker) {
    $categories = App\Models\Category::pluck('id')->toArray();
    $communities = App\Models\Community::pluck('id')->toArray();

    return [
        'category_id'=> $faker->randomElement($categories),
        'community_id'=> $faker->randomElement($communities),
    ];
});
