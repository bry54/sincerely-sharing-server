<?php

use Illuminate\Database\Seeder;

class PostVotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\PostVote::class, 1000)->create()->each( function ($postVote) {
            $p = \App\Models\Post::where('id', '=', $postVote->post_id)->first();
            $postOwner=\App\User::where('id', '=', $p->author->id)->first();
            if ($postVote->up_vote == 1)
                $postOwner->reputation = $postOwner->reputation + 1;
            if ($postVote->down_vote == 1)
                $postOwner->reputation = $postOwner->reputation - 1;

            $postOwner->save();
        });
    }
}
