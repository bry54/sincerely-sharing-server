<?php

use Illuminate\Database\Seeder;

class CommentVotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\CommentVote::class, 1000)->create()->each( function ($commentVote) {
            $p = \App\Models\Comment::where('id', '=', $commentVote->comment_id)->first();
            $commentOwner=\App\User::where('id', '=', $p->author->id)->first();
            if ($commentVote->up_vote == 1)
                $commentOwner->reputation = $commentOwner->reputation + 1;
            if ($commentVote->down_vote == 1)
                $commentOwner->reputation = $commentOwner->reputation - 1;

            $commentOwner->save();
        });
    }
}
