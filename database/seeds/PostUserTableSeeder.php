<?php

use App\Models\Post;
use App\User;
use Illuminate\Database\Seeder;

class PostUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = Post::all();

        User::all()->each( function ($user) use ($posts) {
            $user->savedPosts()->attach(
                $posts->random(rand(0, 80))->pluck('id')->toArray()
            );
        });
    }
}
