<?php

use App\Models\Category;
use App\Models\Community;
use Illuminate\Database\Seeder;

class CategoryCommunityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::all();

        Community::all()->each( function ($community) use ($categories) {
            $community->categories()->attach(
                $categories->random(rand(5, 10))->pluck('id')->toArray()
            );
        });
    }
}
