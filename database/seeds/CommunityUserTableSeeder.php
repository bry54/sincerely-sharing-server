<?php

use App\Models\Community;
use App\User;
use Illuminate\Database\Seeder;

class CommunityUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();

        Community::all()->each( function ($community) use ($users) {
            $community->users()->attach(
                $users->random(rand(30, 100))->pluck('id')->toArray()
            );
        });
    }
}
