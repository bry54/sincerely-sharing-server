<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Comment::class, 500)->create()->each( function ($comment) {
            $comment->comments()->save(factory(App\Models\Comment::class)->make());
        });
    }
}
