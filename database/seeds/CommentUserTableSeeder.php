<?php

use App\Models\Comment;
use App\User;
use Illuminate\Database\Seeder;

class CommentUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $comments = Comment::all();

        User::all()->each( function ($user) use ($comments) {
            $user->savedComments()->attach(
                $comments->random(rand(0, 100))->pluck('id')->toArray()
            );
        });
    }
}
