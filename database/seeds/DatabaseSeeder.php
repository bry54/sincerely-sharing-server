<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CommunitiesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(CategoryCommunityTableSeeder::class);

        $this->call(UsersTableSeeder::class);

        $this->call(CommunityUserTableSeeder::class);

        $this->call(PostsTableSeeder::class);

        $this->call(CommentsTableSeeder::class);

        $this->call(PostUserTableSeeder::class);
        $this->call(CommentUserTableSeeder::class);

        $this->call(CommentVotesTableSeeder::class);
        $this->call(PostVotesTableSeeder::class);
    }
}
