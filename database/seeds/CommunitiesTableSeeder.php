<?php

use App\Models\Category;
use App\Models\Community;
use Illuminate\Database\Seeder;

class CommunitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Community::class, 10)->create()->each( function ($category) {

        });
    }
}
