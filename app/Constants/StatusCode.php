<?php
/**
 * Created by PhpStorm.
 * User: brian
 * Date: 1/18/19
 * Time: 3:03 PM
 */

namespace App\Constants;


class StatusCode
{
    const OK = 200;
    const CREATED  = 201;
    const ACCEPTED = 202;

    const INTERNAL_SERVER_ERROR = 500;
    const SERVICE_UNAVAILABLE = 503;
    const NETWORK_CONNECT_TIMEOUT_ERROR = 599;

    const BAD_REQUEST = 400;
    const UNAUTHORIZED = 401;
    const FORBIDDEN = 403;
    const NOT_FOUND = 404;
    const NOT_ACCEPTABLE = 406;
    const PAYLOAD_TOO_LARGE = 413;
    const UNPROCESSABLE_ENTITY = 422;
}