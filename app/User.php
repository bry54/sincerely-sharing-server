<?php

namespace App;

use App\Models\Comment;
use App\Models\Community;
use App\Models\Post;
use App\Models\OauthAccessToken;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

/**
 * @property mixed created_at
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     * @property string $username
     * @property string $email
     * @property string $password
     * @property string id
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'reputation','password',
    ];

    protected $dates = ['created_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $appends = ['readable_age','posts_count','comments_count'];

    public function AauthAcessToken(){
        return $this->hasMany(OauthAccessToken::class);
    }

    /**
     * Get the posts by the user(author).
     */
    public function posts()
    {
        return $this->hasMany(Post::class, 'author_id', 'id');
    }

    /**
     * Get the comments by the user(author).
     */
    public function comments()
    {
        return $this->hasMany(Comment::class, 'author_id', 'id')
            ->with('post.community');
    }

    /**
     * The communities that belongs to the user.
     */
    public function communities()
    {
        return $this->belongsToMany(Community::class, 'community_users','user_id','community_id');
    }

    /**
     * The posts that were saved by user.
     */
    public function savedPosts()
    {
        return $this->belongsToMany(Post::class, 'post_users','user_id','post_id');
    }

    /**
     * The posts that were saved by user.
     */
    public function savedComments()
    {
        return $this->belongsToMany(Comment::class, 'comment_users','user_id','comment_id');
    }

    public function getReadableAgeAttribute()
    {
        return $this->created_at->diffForHumans(null, true).'';
    }
    
    public function getCommentsCountAttribute()
    {
        return $this->comments()->count();
    }

    public function getPostsCountAttribute()
    {
        return $this->posts()->count();
    }
}
