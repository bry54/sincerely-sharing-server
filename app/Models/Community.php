<?php

namespace App\Models;

use App\Pivots\CommunityUser;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Community extends Model
{
    protected $appends = ['user_ids', 'categories'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','summary', 'password'
    ];

    /**
     * Get the comments by the user(author).
     */
    public function posts()
    {
        return $this->hasMany(Post::class, 'community_id', 'id');
    }

    /**
     * The users that belongs to the community.
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'community_users','community_id','user_id');
    }

    /**
     * The categories that belongs to the community.
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_communities','category_id','community_id');
    }

    public function subscriberCount()
    {
        return $this->hasOne(CommunityUser::class)
            ->selectRaw('community_id, count(*) as count')
            ->groupBy('community_id');
    }

    public function getUserIdsAttribute()
    {
        return $this->users()->get()->pluck('pivot.user_id');
    }

    public function getCategoriesAttribute()
    {
        return $this->categories()->get();
    }
}
