<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CommunityVote;

class CommunityRequest extends Model
{
    protected $fillable = [
        'title', 'body', 'fif_exlusive'
    ];

    protected $appends = [
        'up_voters_ids',
        'down_voters_ids',
        'up_votes_count',
        'down_votes_count'
    ];

    /**
     * Get the votes associated with the post.
     */
    public function votes()
    {
        return $this->hasMany(CommunityVote::class, 'community_request_id', 'id');
    }

    public function getUpVotersIdsAttribute()
    {
        return $this->votes()->where('up_vote','=',1)->get()->pluck('user_id');
    }

    public function getDownVotersIdsAttribute()
    {
        return $this->votes()->where('down_vote','=',1)->get()->pluck('user_id');
    }

    public function getUpVotesCountAttribute()
    {
        return $this->votes()->where('up_vote','=',1)->count();
    }

    public function getDownVotesCountAttribute()
    {
        return $this->votes()->where('down_vote','=',1)->count();
    }
}
