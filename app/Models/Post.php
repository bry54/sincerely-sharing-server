<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property Carbon created_at
 */
class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'body', 'author_id', 'category_id', 'community_id', 'confidence', 'hotness', 'is_archived','is_deleted'
    ];

    protected $appends = [
        'votes_difference',
        'followers_ids',
        'voters_ids',
        'up_voters_ids',
        'down_voters_ids',
        'community',
        'category',
        'author',
        'comments_count',
        'votes_count'
    ];

    /**
     * Get the comments for the post.
     */
    public function allComments()
    {
        return $this->hasMany(Comment::class, 'post_id', 'id');
    }

    /**
     * Get the visible comments for the post.
     */
    public function comments()
    {
        return $this->allComments()
            ->where('is_deleted','=', 0)
            ->where('is_archived','=', 0);
    }

    /**
     * Get the votes associated with the post.
     */
    public function votes()
    {
        return $this->hasMany(PostVote::class, 'post_id', 'id');
    }

    /**
     * Get the owner(author) for the post.
     */
    public function author()
    {
        return $this->belongsTo(User::class,'author_id', 'id');
    }

    /**
     * Get the owner(community) for the post.
     */
    public function community()
    {
        return $this->belongsTo(Community::class,'community_id', 'id');
    }

    /**
     * Get the owner(category) for the post.
     */
    public function category()
    {
        return $this->belongsTo(Category::class,'category_id', 'id');
    }

    /**
     * The users that saved the post.
     */
    public function followingUsers()
    {
        return $this->belongsToMany(User::class, 'post_users','post_id','user_id');
    }

    public function getVotesDifferenceAttribute()
    {
        $upVotes = $this->votes()->where('up_vote','=',1)->count();
        $downVotes = $this->votes()->where('down_vote','=',1)->count();

        return $upVotes - $downVotes;
    }

    public function getFollowersIdsAttribute()
    {
        return $this->followingUsers()->get()->pluck('pivot.user_id');
    }

    public function getVotersIdsAttribute()
    {
        return $this->votes()->get()->pluck('user_id');
    }

    public function getUpVotersIdsAttribute()
    {
        return $this->votes()->where('up_vote','=',1)->get()->pluck('user_id');
    }

    public function getDownVotersIdsAttribute()
    {
        return $this->votes()->where('down_vote','=',1)->get()->pluck('user_id');
    }

    public function getCommunityAttribute()
    {
        return $this->community()->first();
    }

    public function getCategoryAttribute()
    {
        return $this->category()->first();
    }

    public function getAuthorAttribute()
    {
        return $this->author()->first();
    }

    public function getCommentsCountAttribute()
    {
        return $this->comments()->count();
    }

    public function getVotesCountAttribute()
    {
        return $this->votes()->count();
    }
}
