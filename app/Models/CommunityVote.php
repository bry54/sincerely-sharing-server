<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommunityVote extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','community_request_id','up_vote', 'down_vote',
    ];
}
