<?php

namespace App\Models;

use App\Pivots\CategoryCommunity;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The communities that belongs to the category.
     */
    public function communities()
    {
        return $this->belongsToMany(Community::class, 'category_communities','community_id','category_id');
    }

}
