<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property Carbon created_at
 */
class Comment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'body', 'author_id', 'post_id', 'parent_id','confidence', 'hotness', 'is_archived','is_deleted'
    ];

    protected $appends = [
        'comments_count', 
        'votes_count',
        'author', 
        'followers_ids',
        'voters_ids', 
        'up_voters_ids',
        'down_voters_ids',
        'parent_comment',
        'parent_post',
    ];

    protected $dates=['created_at'];

    /**
     * Get the post that owns the comment.
     */
    public function post()
    {
        return $this->belongsTo(Post::class,'post_id', 'id');
    }

    /**
     * Get the user(author) that owns the comment.
     */
    public function author()
    {
        return $this->belongsTo(User::class,'author_id', 'id');
    }

    /**
     * Get the vote record associated with the post.
     */
    public function votes()
    {
        return $this->hasMany(CommentVote::class, 'comment_id', 'id');
    }

    /**
     * Get the inner comments for the comment.
     */
    public function childComments()
    {
        return $this->hasMany(Comment::class, 'parent_id', 'id');
    }

    /**
     * Recursively get the inner comments for the comment.
     */
    public function comments()
    {
        return $this->childComments()
            ->with('comments','author','votes')
            ->where('is_archived','=',0)
            ->where('is_deleted','=',0);
    }

    /**
     * Recursively get the inner comments for the comment.
     */
    public function parent()
    {
        return $this->hasOne(Comment::class, 'id', 'parent_id');
    }

    /**
     * The users that saved the comment.
     */
    public function followingUsers()
    {
        return $this->belongsToMany(User::class, 'comment_users','comment_id','user_id');
    }

    public function getCommentsCountAttribute()
    {
        return $this->comments()->count();
    }

    public function getVotesCountAttribute()
    {
        return $this->votes()->count();
    }

    public function getAuthorAttribute()
    {
        return $this->author()->first();
    }

    public function getVotersIdsAttribute()
    {
        return $this->votes()->get()->pluck('user_id');
    }

    public function getUpVotersIdsAttribute()
    {
        return $this->votes()->where('up_vote','=',1)->get()->pluck('user_id');
    }

    public function getDownVotersIdsAttribute()
    {
        return $this->votes()->where('down_vote','=',1)->get()->pluck('user_id');
    }

    public function getFollowersIdsAttribute()
    {
        return $this->followingUsers()->get()->pluck('pivot.user_id');
    }

    public function getParentCommentAttribute()
    {
        return $this->parent()->first();
    }

    public function getParentPostAttribute()
    {
        return $this->post()->first();
    }
}
