<?php

namespace App\Exceptions;

use Exception;

class ReportableException extends Exception
{

    public function reporting()
    {

    }
    public function render()
    {
        return abort($this->getCode(), $this->getMessage());
    }
}
