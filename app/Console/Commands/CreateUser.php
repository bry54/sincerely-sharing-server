<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates the default user to own deleted content';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //'username', 'email', 'reputation','password',
        User::create([
            'username' => 'item-rescue',
            'email' => 'i-rescue-items@fif.org',
            'reputation' => 0,
            'password' => bcrypt('IOwnOrphanedItems')
        ]);

        $this->info("User to own orphaned items (post and comments) was created");
    }
}
