<?php
/**
 * Created by PhpStorm.
 * User: brian
 * Date: 1/17/19
 * Time: 9:15 AM
 */

namespace App\Http\Utilities;


use Carbon\Carbon;

class RankingCalculator
{
    public function calculateHotness($upVotes, $downVotes, $date) {
        $DATE_OF_LAUNCH = 1547424000;

        $s = $upVotes - $downVotes;
        $order = log10(max(abs($s), 1));
        $timeSinceEpoch = self::epochSeconds($date);

        if ($s > 0) $sign = 1;
        elseif ($s < 0) $sign = -1;
        else $sign = 0;

        $seconds = $timeSinceEpoch-$DATE_OF_LAUNCH;

        return round($sign * $order + $seconds / 45000, 7);
    }

    public function  calculateConfidence($ups, $downs) {
        $totalVotes = $ups + $downs;

        if ($totalVotes == 0) return 0;

        $z = 1.281551565545; //80% confidence
        $phat = (float)$ups / $totalVotes;

        $left = $phat + 1/(2*$totalVotes)*$z*$z;
        $right = $z*sqrt($phat*(1-$phat)/$totalVotes + $z*$z/(4*$totalVotes*$totalVotes));
        $under = 1+1/$totalVotes*$z*$z;

        return ($left - $right) / $under;

    }

    private  function epochSeconds($timestamp)
    {
        $epoch = Carbon::create(1970,01,01,00,00,00);
        $unix = Carbon::createFromTimeString($timestamp);

        $td = $epoch->diffInSeconds($unix);

        return $td;
    }
}