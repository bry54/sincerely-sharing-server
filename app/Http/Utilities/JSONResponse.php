<?php

namespace App\Utilities;

/**
 * Class JSONResponse
 * @package App\Utilities
 */
class JSONResponse extends \Illuminate\Http\JsonResponse
{
    /**
     * JSONResponse constructor.
     * @param string $title
     * @param string $message
     * @param int $status
     * @param array $errors
     * @param array $data
     */
    public function __construct($title = '', $message = '', $status = 200, $errors = [], $data = [])
    {
        return parent::__construct([
            'title' => $title,
            'message' => $message,
            'errors' => $errors,
            'data' => $data
        ], $status );
    }

}