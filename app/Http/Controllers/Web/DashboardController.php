<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Raulr\GooglePlayScraper\Scraper;


class DashboardController extends Controller
{
    public function index()
    {
        $androidScraper = new Scraper();
        $androidStats = $androidScraper->getApp('tr.edu.ciu.ciumobile');

        return view('dashboard')->with('android', $androidStats);
    }
}