<?php

namespace App\Http\Controllers\Web;

use App\Http\Utilities\RankingCalculator;
use App\Models\Comment;
use App\Models\Community;
use App\Models\Post;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function test(Request $request)
    {
        dd(User::where('id', '=', 89)->with('communities')->first());
        //$this->normaliseRankings();
    }

    private function normaliseRankings()
    {
        $ranksCalculator = new RankingCalculator();
        foreach (Post::all() as $post){
            $total = $post->votes->count();
            $up = $post->votes->where('up_vote','=',1)->count();
            $down = $post->votes->where('down_vote','=',1)->count();
            $confidence = $ranksCalculator->calculateConfidence($up, $down);

            $hotness = $ranksCalculator->calculateHotness($up, $down, $post->created_at);

            $post->confidence = $confidence;
            $post->hotness = $hotness;
            $post->save();
        }

        foreach (Comment::all() as $comment){
            $total = $comment->votes->count();
            $up = $comment->votes->where('up_vote','=',1)->count();
            $down = $comment->votes->where('down_vote','=',1)->count();
            $confidence = $ranksCalculator->calculateConfidence($up, $down);
            $hotness = $ranksCalculator->calculateHotness($up, $down, $comment->created_at);

            $comment->confidence = $confidence;
            $comment->hotness = $hotness;
            $comment->save();
        }
    }
}
