<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Post;

class PostsController extends Controller
{
    public function index()
    {
        return view('posts.posts-management');
    }

    public function getPosts()
    {
        return datatables(Post::all())->toJson();
    }

    public function addPost(Request $request)
    {
        
    }

    public function deletePost(Request $request)
    {
        
    }
}
