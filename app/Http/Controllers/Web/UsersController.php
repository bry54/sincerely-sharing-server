<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\User;

class UsersController extends Controller
{
    public function index()
    {
        return view('users.users-management');
    }

    public function getUsers()
    {
        return datatables(User::all())->toJson();
    }

    public function addUser(Request $request)
    {
        
    }

    public function deleteUser(Request $request)
    {
        
    }
}
