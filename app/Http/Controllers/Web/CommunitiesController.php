<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Community;
use App\Models\CommunityRequest;

class CommunitiesController extends Controller
{
    public function index()
    {
        return view('communities.community-management');
    }

    public function getCommunities()
    {
        return datatables(Community::all())->toJson();
    }

    public function addCommunity(Request $request)
    {
        $community = null;
        $community_id = $request->get('community_id');

        $community_name= $request->get('community_name');
        $community_summary = $request->get('community_summary');
        $community_password = $request->get('community_password');
        $fif_exclusive = $request->get('fif_exclusive');

        $community = Community::updateOrCreate(['id' => $community_id],
            [
                'name'=>$community_en,
                'summary'=>$community_tr,
                'password' => $community_code,
                'fif_exclusive'=> $fif_exclusive
            ]);
    }

    public function deleteCommunity(Request $request)
    {
        $community_id = $request->get('community_id');

        $community = Community::where('id', $community_id)->first();

        if($community){
            $community->delete();
            return back()->with('success','Successfully deleted group '.$community->definition);
        } else {
            return back()->with('error','Failed to delete group.');
        }
    }
}
