<?php

namespace App\Http\Controllers\Mobile;

use App\Models\Category;
use App\Models\Comment;
use App\Models\Community;
use App\Models\Post;
use App\Pivots\PostUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PostsController extends Controller
{
    const DATE_ORDERING = "DATE";
    const CONFIDENCE_ORDERING = "CONFIDENCE";
    const HOTNESS_ORDERING = "HOTNESS";

    public function getAllPosts(Request $request)
    {
        $posts = Post::all();
        return $posts;
    }

    public function getAllCategories(Request $request)
    {
        $categories = Category::all();
        return $categories;
    }

    public function getMyPosts(Request $request)
    {
        $author = auth()->user();
        $posts = $author->posts;
        return $posts;
    }

    public function getPosts(Request $request)
    {
        $community_id = $request->get('community_id');
        $ordering = $request->get('order_criterion');

        if ($community_id) {
            $feeds = $this->getPostsFromSpecificCommunity($community_id, $ordering);
        } else {
            $feeds = $this->getPostsFromSubscribedCommunities($ordering);
        }

        return response()->json([
            'feeds' => $feeds,
        ]); 
    }

    public function getPostsFromSpecificCommunity($community_id, $ordering)
    {
        $posts = Post::where('community_id','=' ,$community_id)
            ->where('is_deleted','=', 0)
            ->where('is_archived','=', 0)
            ->get();

        if ($ordering == self::DATE_ORDERING)
            $posts = $posts->sortByDesc('created_at');
        if ($ordering == self::CONFIDENCE_ORDERING)
            $posts = $posts->sortByDesc('confidence');
        if ($ordering == self::HOTNESS_ORDERING)
            $posts = $posts->sortByDesc('hotness');

        return $posts->flatten();
    }

    public function getPostsFromSubscribedCommunities($ordering)
    {
        $user = auth()->user();
        $communities = $user->communities;

        /** @var Collection $posts */
        $posts = Post::whereIn('community_id', $communities->pluck('id'))
            ->where('is_deleted','=', 0)
            ->where('is_archived','=', 0)
            ->get();

        if ($ordering == self::DATE_ORDERING)
            $posts = $posts->sortByDesc('created_at');
        if ($ordering == self::CONFIDENCE_ORDERING)
            $posts = $posts->sortByDesc('confidence');
        if ($ordering == self::HOTNESS_ORDERING)
            $posts = $posts->sortByDesc('hotness');

        return $posts->flatten();
    }

    public function getPostsFromCategory(Request $request)
    {
        $community_id = 5;
        $category_id = 3;
        $posts = Post::where([
            ['community_id','=', $community_id],
            ['category_id','=', $category_id],
        ])->get();
        return $posts;
    }

    public function submitPost(Request $request)
    {
        $author = auth()->user();

        try {
            $post = Post::where([
                ['id','=', $request->get('post_id')],
                ['author_id','=', $author->id],
            ])->firstOrFail();
            $post->title = $request->get('title');
            $post->body = $request->get('body');
            $post->category_id = $request->get('category');
            $post->community_id = $request->get('community');
            $post->save();
        } catch (ModelNotFoundException $ex) {
            $post = new Post([
                'author_id'=> $author->id,
                'title'=>$request->get('title'),
                'body'=>$request->get('body'),
                'category_id' => $request->get('category'),
                'community_id' => $request->get('community'),
            ]);
            $post->save();
        }

        return response()->json([
            'feed' => $post,
        ]);
    }

    public function getPostDetail(Request $request)
    {
        $ordering = $request->get('order_criterion');
        $post_id = $request->get('post_id');

        $post = Post::where('id','=', $post_id)
        ->with(['comments'])
        ->first();

        if ($ordering == self::DATE_ORDERING)
            $post->comments = $post->comments->sortByDesc('created_at');
        if ($ordering == self::CONFIDENCE_ORDERING)
            $post->comments = $post->comments->sortByDesc('confidence');
        if ($ordering == self::HOTNESS_ORDERING)
            $post->comments = $post->comments->sortByDesc('hotness');

        return response()->json([
            'feed' => $post,
        ]); 
    }

    public function followPost(Request $request)
    {
        $user = auth()->user();
        $post_id = $request->get('post_id');

        $existingFollower = PostUser::where('post_id','=',$post_id)
        ->where('user_id','=',$user->id)->first();

        if ($existingFollower) {
            $existingFollower->delete();
        }
        else {
            $newFollowing = new PostUser(['user_id' => $user->id, 'post_id' => $post_id]);
            $newFollowing->save();
        }

        $post = Post::where('id','=',$post_id)
        ->first();

        return response()->json([
            'followers'=>$post->followers_ids,
        ]);
    }

    public function editPost(Request $request)
    {
        $user = auth()->user();
        $post_id = $request->get('post_id');

        $post = Post::find($post_id);

        $post->title = $request->get('title');
        $post->body = $request->get('body');
        $post->category_id = $request->get('category');
        $post->community_id = $request->get('community');

        $post->save();

        $updatedPost = Post::with(['comments'])->find($post_id);

        return response()->json([
            'feed' => $updatedPost,
        ]); 
    }

    public function archivePost(Request $request)
    {
        $user = auth()->user();
        $post_id = $request->get('post_id');

        $post = Post::where('id','=',$post_id)->first();
        $current_status = $post->is_archived;
        $new_status = $current_status === 0 ? 1 : 0;

        $post->is_archived = $new_status;
        $post->save();

        return response()->json([
            'feed'=>$post,
        ]);
    }

    public function deletePost(Request $request)
    {
        $user = auth()->user();
        $post_id = $request->get('post_id');

        $post = Post::where('id','=',$post_id)->first();
        $post->is_deleted = 1;
        $post->save();

        return response()->json([
            'success'=>true,
            'message'=>'Deleted post',
        ]);
    }

    public function getPostComments(Request $request)
    {
        $ordering = $request->get('order_criterion');
        $post_id = $request->get('post_id');

        $comments = Comment::where('post_id','=',$post_id)
        ->with('votes', 'comments', 'author')
        ->withCount('comments')
        ->get();

        if ($ordering == self::DATE_ORDERING)
            $comments = $comments->sortByDesc('created_at');
        if ($ordering == self::CONFIDENCE_ORDERING)
            $comments = $comments->sortByDesc('confidence');
        if ($ordering == self::HOTNESS_ORDERING)
            $comments = $comments->sortByDesc('hotness');

        return $comments->flatten();
    }
}
