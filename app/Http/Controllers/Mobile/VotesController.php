<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Requests\VoteRequest;
use App\Http\Utilities\RankingCalculator;
use App\Models\Comment;
use App\Models\CommentVote;
use App\Models\Post;
use App\Models\PostVote;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VotesController extends Controller
{
    public function voteForPost(VoteRequest $request)
    {
        $user = auth()->user();
        $duplicateVote = null;

        $statsCalculator = new RankingCalculator();
        $post_id = $request->get('post_id');
        $up = $request->get('up_vote');
        $down = $request->get('down_vote');
        $post = Post::find($post_id);

        $existingVote = PostVote::where('post_id','=',$post->id)
            ->where('user_id', '=', $user->id)
            ->first();

        if ($existingVote)
            $duplicateVote = $existingVote->user_id == $user->id &&
                $existingVote->post_id == $post_id &&
                $existingVote->up_vote == $up &&
                $existingVote->down_vote == $down;

        PostVote::updateOrCreate(
            ['user_id' => $user->id, 'post_id' => $post->id],
            ['up_vote' => $up, 'down_vote'=>$down]
        );

        if ($duplicateVote){}
        else {
            $postOwner = $post->author;
            if ($up == 1)
                $postOwner->reputation = $postOwner->reputation + 1;
            if ($down == 1)
                $postOwner->reputation = $postOwner->reputation - 1;

            $postOwner->save();
        }

        $total = $post->votes->count();
        $up = $post->votes->where('up_vote','=',1)->count();
        $down = $post->votes->where('down_vote','=',1)->count();
        $confidence = $statsCalculator->calculateConfidence($up, $down);
        $hotness = $statsCalculator->calculateHotness($up, $down, $post->created_at);

        $post->confidence = $confidence;
        $post->hotness = $hotness;
        $post->save();

        $post = Post::where('id','=',$post_id)
            ->first();

        return response()->json([
            'up_votes'=>$post->up_voters_ids,
            'down_votes'=>$post->down_voters_ids,
        ]);
    }

    public function voteForComment(VoteRequest $request)
    {
        $user = auth()->user();
        $duplicateVote = null;

        $statsCalculator = new RankingCalculator();
        $comment_id = $request->get('comment_id');
        $up = $request->get('up_vote');
        $down = $request->get('down_vote');
        $comment = Comment::find($comment_id);

        $existingVote = CommentVote::where('comment_id','=',$comment->id)
            ->where('user_id', '=', $user->id)
            ->first();

        if ($existingVote)
            $duplicateVote = $existingVote->user_id == $user->id &&
                $existingVote->post_id == $comment_id &&
                $existingVote->up_vote == $up &&
                $existingVote->down_vote == $down;

        $vote = CommentVote::updateOrCreate(
            ['user_id' => $user->id, 'comment_id' => $comment_id],
            ['up_vote' => $up, 'down_vote'=>$down]
        );

        if($duplicateVote){}
        else {
            $commentOwner = $comment->author;
            if ($up == 1)
                $commentOwner->reputation = $commentOwner->reputation + 1;
            if ($down == 1)
                $commentOwner->reputation = $commentOwner->reputation - 1;

            $commentOwner->save();
        }

        $total = $comment->votes->count();
        $up = $comment->votes->where('up_vote','=',1)->count();
        $down = $comment->votes->where('down_vote','=',1)->count();
        $confidence = $statsCalculator->calculateConfidence($up, $down);
        $hotness = $statsCalculator->calculateHotness($up, $down, $comment->created_at);

        $comment->confidence = $confidence;
        $comment->hotness = $hotness;
        $comment->save();

        $updated_comment = Comment::where('id','=', $comment_id)
            ->first();

        return response()->json([
            'up_votes'=>$updated_comment->up_voters_ids,
            'down_votes'=>$updated_comment->down_voters_ids,
        ]);
    }
}
