<?php

namespace App\Http\Controllers\Mobile;

use App\Models\Comment;
use App\Models\Post;
use App\Pivots\CommentUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CommentsController extends Controller
{

    const DATE_ORDERING = "DATE";
    const CONFIDENCE_ORDERING = "CONFIDENCE";
    const HOTNESS_ORDERING = "HOTNESS";

    public function getCommentDetail(Request $request)
    {
        $ordering = $request->get('order_criterion');
        $comment_id = $request->get('comment_id');

        $comment = Comment::where('id','=', $comment_id)
            ->with(['comments','parent'])
            ->first();

        if ($ordering == self::DATE_ORDERING)
            $comment->comments = $comment->comments->sortByDesc('created_at');
        if ($ordering == self::CONFIDENCE_ORDERING)
            $comment->comments = $comment->comments->sortByDesc('confidence');
        if ($ordering == self::HOTNESS_ORDERING)
            $comment->comments = $comment->comments->sortByDesc('hotness');

        return response()->json([
            'comment'=>$comment,
        ]);
    }

    public function submitCommentForPost(Request $request)
    {
        $author = auth()->user();
        $comment_id = $request->get('comment_id');
        $post_id = $request->get('post_id');
        $comment_body = $request->get('comment_body');

        //dd(Comment::where('id', '=',$comment_id)->first());
        
        try {
            $comment = Comment::where([
                ['id','=', $comment_id],
                ['post_id','=', $post_id],
                ['author_id','=', $author->id],
            ])->firstOrFail();
            $comment->body = $comment_body;
            $comment->save();

            return response()->json([
                'comment' => $comment,
            ]);
        } catch (ModelNotFoundException $ex) {
            $comment = new Comment([
                'body'=>$comment_body,
                'author_id'=> $author->id,
                'post_id'=>$post_id
            ]);
            $comment->save();

            $post = Post::where('id','=', $post_id)
                ->with(['comments'])
                ->first();

            return response()->json([
                'feed' => $post,
            ]);
        }
    }

    public function submitCommentForComment(Request $request)
    {
        $parent_id = $request->get('parent_id');
        $comment_body = $request->get('comment_body');
        $author = auth()->user();

        try {
            $comment = Comment::where([
                ['id','=', $request->get('comment_id')],
                ['parent_id','=', $parent_id],
                ['author_id','=', $author->id],
            ])->firstOrFail();
            $comment->body = $comment_body;
            $comment->save();

            return response()->json([
                'comment' => $comment,
            ]);
        } catch (ModelNotFoundException $ex) {
            $nComment = new Comment([
                'body'=>$comment_body,
                'parent_id'=>$parent_id,
                'author_id'=> $author->id,
            ]);
            $nComment->save();

            $comment = Comment::where('id','=', $parent_id)
                ->with(['comments'])
                ->first();

            return response()->json([
                'comment'=>$comment,
            ]);
        }
    }

    public function editComment(Request $request)
    {
        $user = auth()->user();
        $post_id = $request->get('post_id');
        $parent_id = $request->get('parent_id');
        $comment_id = $request->get('comment_id');

        if ($post_id) {
            $comment = Comment::where('id','=', $comment_id)->where('post_id', '=', $post_id)->first();        
        }

        if ($parent_id) {
            $comment = Comment::where('id','=', $comment_id)->where('parent_id', '=', $parent_id)->first();        
        } 

        $comment->body = $request->get('body');

        $comment->save();

        return response()->json([
            'comment'=>$comment,
        ]);
    }

    public function deleteComment(Request $request)
    {
        $user = auth()->user();
        $comment_id = $request->get('comment_id');
        $comment = Comment::find($comment_id);
        $comment->delete();

        return response()->json([
            'success'=>true,
            'message'=>'Deleted Comment',
        ]);
    }

    public function followComment(Request $request)
    {
        $user = auth()->user();
        $comment_id = $request->get('comment_id');

        $existingFollower = CommentUser::where('comment_id','=',$comment_id)
            ->where('user_id','=',$user->id)->first();

        if ($existingFollower) {
            $existingFollower->delete();
        }
        else {
            $newFollowing = new CommentUser(['user_id' => $user->id, 'comment_id' => $comment_id]);
            $newFollowing->save();
        }

        $comment = Comment::where('id','=',$comment_id)->first();

        return response()->json([
            'followers'=>$comment->followers_ids,
        ]);
    }

    public function archiveComment(Request $request)
    {
        $user = auth()->user();
        $comment_id = $request->get('comment_id');

        $comment = Comment::where('id','=',$comment_id)->first();
        $current_status = $comment->is_archived;
        $new_status = $current_status === 0 ? 1 : 0;

        $comment->is_archived = $new_status;
        $comment->save();

        return response()->json([
            'comment'=>$comment,
        ]);
    }

    public function deleteComent(Request $request)
    {
        $user = auth()->user();
        $comment_id = $request->get('comment_id');

        $comment = Comment::where('id','=',$comment_id)->first();
        $comment->is_deleted = 1;
        $comment->author_id = 1;

        return response()->json([
            'success'=>true,
            'message'=>'Deleted comment',
        ]);
    }

    //Redudant functions
    public function fetchCommentComments(Request $request)
    {
        $ordering = $request->get('order_criterion');
        $comment_id = $request->get('comment_id');

        $comments = Comment::where('parent_id','=', $comment_id)
            ->with('author','votes','comments')
            ->withCount('comments')
            ->get();

        if ($ordering == self::DATE_ORDERING)
            $comments->comments = $comments->sortByDesc('created_at');
        if ($ordering == self::CONFIDENCE_ORDERING)
            $comments->comments = $comments->sortByDesc('confidence');
        if ($ordering == self::HOTNESS_ORDERING)
            $comments->comments = $comments->sortByDesc('hotness');

        return $comments->flatten();
    }

    public function getMyComments(Request $request)
    {
        $author = auth()->user();
        $comments = $author->comments;
        return $comments;
    }

}
