<?php

namespace App\Http\Controllers\Mobile;

use App\Constants\StatusCode;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegistrationRequest;
use App\Pivots\CommunityUser;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Handles Registration Request
     *
     * @param RegistrationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegistrationRequest $request)
    {
        $user = User::create([
            'username' => $request->get('username'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ]);

        $subscription = new CommunityUser;
        $subscription->community_id = 1;
        $subscription->user_id = $user->id;
        $subscription->save();

        $token = $user->createToken('sincerelysharing')->accessToken;

        return response()->json(['title' => 'Authorised', 'message'=>'Successful Login',
            'payload'=> [
                'token' => $token,
                'user'=>$user
            ]
        ], StatusCode::OK);
    }

    /**
     * Handles Login Request
     *
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $credentials = [
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ];

        if (auth()->attempt($credentials)) {
            $logged_in_user=Auth::user();
            $token = auth()->user()->createToken('sincerelysharing')->accessToken;
            return response()->json(['title' => 'Authorised', 'message'=>'Successful Login',
                'payload'=> [
                    'token' => $token,
                    'user'=>$logged_in_user
                ]
            ], StatusCode::OK);
        } else {
            return response()->json(['title' => 'Failed Login', 'message'=>'Invalid login credentials',
                'payload'=>$credentials],
                StatusCode::UNAUTHORIZED);
        }
    }

    public function logout(Request $request){
        if (Auth::check()) {
           Auth::logout();
        }
    }
}
