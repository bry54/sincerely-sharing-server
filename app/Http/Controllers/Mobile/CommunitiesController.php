<?php

namespace App\Http\Controllers\Mobile;

use App\Models\Community;
use App\Models\CommunityRequest;
use App\Models\CommunityVote;
use App\Pivots\CommunityUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CommunitiesController extends Controller
{
    public function getAllCommunities(Request $request)
    {
        $communities = Community::withCount('users')->get();
        $requested = CommunityRequest::all();

        return response()->json([
            'available' => $communities,
            'requested' => $requested,
        ]);
    }

    public function getSubscribedCommunities()
    {
        $user = auth()->user();
        return $user->communities;
    }
    public function toggleCommunitySubscription (Request $request){
        $subscribeMessage = 'Message';
        $subscribeResult = false;

        $user = auth()->user();
        $community_id = $request->get('community_id');
        $password = $request->get('password');
        $community = Community::find($community_id);

        try {
            $subscription = CommunityUser::where([
                ['community_id','=', $community_id],
                ['user_id','=', $user->id],
            ])->firstOrFail();
            $subscription->delete();
            $subscribeMessage = 'Unsubscribed to community';
            $subscribeResult = true;
        } catch (ModelNotFoundException $ex) {
            if ($community->password){
                if ($password) {
                    if (Hash::check($password, $community->password)) {
                        $new_subscription = new CommunityUser([
                            'community_id' => $community_id,
                            'user_id' => $user->id
                        ]);
                        $new_subscription->save();
                        $subscribeMessage = 'Subscribed to community';
                        $subscribeResult = true;
                    } else {
                        $subscribeMessage = 'Incorrect Password';
                        $subscribeResult = false;
                    }
                } else {
                    $subscribeMessage = 'Missing Password';
                    $subscribeResult = false;
                }
            } else {
                $new_subscription = new CommunityUser([
                    'community_id' => $community_id,
                    'user_id' => $user->id
                ]);
                $new_subscription->save();
                $subscribeMessage = 'Subscribed to community';
                $subscribeResult = true;
            }
        }

        $communityData = Community::where('id','=', $community_id)
            ->withCount('users')
            ->first();

        return response()->json([
            'community' => $communityData,
            'message' => $subscribeMessage,
            'success' => $subscribeResult
        ]);
    }

    public function requestCommunity (Request $request){
        $req = new CommunityRequest([
            'title'=>$request->get('title'),
            'body'=>$request->get('body'),
            'fif_exclusive'=>$request->get('fif_exclusive')
        ]);
        $req->save();

        return response()->json([
            'requested' => $req,
        ]);
    }

    public function voteRequestCommunity(Request $request)
    {
        $user = auth()->user();
        $duplicateVote = null;

        $community_id = $request->get('community_id');
        $up = $request->get('up_vote');
        $down = $request->get('down_vote');
        $cr = CommunityRequest::find($community_id);

        CommunityVote::updateOrCreate(
            ['user_id' => $user->id, 'community_request_id' => $cr->id],
            ['up_vote' => $up, 'down_vote'=>$down]
        );

        $cr = CommunityRequest::where('id','=',$community_id)->first();

        return response()->json([
            'requested' => $cr,
        ]);
    }

    //Redudant 
    public function leaveCommunity(Request $request)
    {
        $user = auth()->user();
        $community_id = $request->get('community_id');

        $subscription = CommunityUser::where([
            ['community_id','=', $community_id],
            ['user_id','=', $user->id],
        ]);
        $subscription->delete();

        $communities = Community::withCount('users')->get();
        return $communities;
    }

    public function subscribeToCommunity(Request $request)
    {
        $subscribeMessage = 'Successfully Joined';
        $subscribeResult = true;
        $user = auth()->user();
        $community_id = $request->get('community_id');
        $password = $request->get('password');
        $community = Community::find($community_id);

        if ($community->password){
            if ($password) {
                if (Hash::check($password, $community->password)) {
                    $new_subscription = new CommunityUser([
                        'community_id' => $community_id,
                        'user_id' => $user->id
                    ]);
                    $new_subscription->save();
                } else {
                    $subscribeMessage = 'Incorrect Password';
                    $subscribeResult = false;
                }
            } else {
                $subscribeMessage = 'Missing Password';
                $subscribeResult = false;
            }
        } else {
            $new_subscription = new CommunityUser([
                'community_id' => $community_id,
                'user_id' => $user->id
            ]);
            $new_subscription->save();
        }

        $communities = Community::withCount('users')->first();
        return $communities;
    }
}
