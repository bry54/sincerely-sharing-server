<?php

namespace App\Http\Controllers\Mobile;

use App\User;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function getMyProfile()
    {
        $user = auth()->user();

        $profile = User::where('id','=',$user->id)
            ->with('posts','comments','savedPosts', 'savedComments')
            ->first();

        return response()->json([
            'profile' => $profile,
        ]);
    }
}
