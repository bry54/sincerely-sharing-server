<?php

namespace App\Pivots;

use Illuminate\Database\Eloquent\Model;

class PostUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id','user_id'
    ];
}
