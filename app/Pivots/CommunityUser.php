<?php

namespace App\Pivots;

use Illuminate\Database\Eloquent\Model;

class CommunityUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'community_id','user_id'
    ];

}
