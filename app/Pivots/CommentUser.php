<?php

namespace App\Pivots;

use Illuminate\Database\Eloquent\Model;

class CommentUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'comment_id','user_id'
    ];
}
