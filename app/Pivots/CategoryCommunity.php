<?php

namespace App\Pivots;

use Illuminate\Database\Eloquent\Model;

class CategoryCommunity extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id','community_id',
    ];

}
