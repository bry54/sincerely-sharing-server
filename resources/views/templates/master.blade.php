<!DOCTYPE html>

<html lang="en" >
<!-- begin::Head -->
@include('layouts.head')
<!-- end::Head -->
<!-- end::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <!-- BEGIN: Header -->
    @include('layouts.header')
    <!-- END: Header -->

    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <!-- BEGIN: Left Aside -->
        @include('layouts.side-menu')
        <!-- END: Left Aside -->

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <!-- BEGIN: Subheader -->
            {{--<div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h2 class="m-subheader__title m-subheader__title--separator">
                            Dashboard
                        </h2>
                    </div>
                </div>
            </div>--}}
            <!-- END: Subheader -->
            @yield('content')
        </div>
    </div>
    <!-- end:: Body -->

    <!-- begin::Footer -->
    @include('layouts.footer')
    <!-- end::Footer -->
</div>
<!-- end:: Page -->

<!-- begin::Scroll Top -->
<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
    <i class="la la-arrow-up"></i>
</div>
<!-- end::Scroll Top -->

<!--begin::Base Scripts -->
<script src="{{url('base-theme/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
<script src="{{url('base-theme/assets/demo/demo3/base/scripts.bundle.js')}}" type="text/javascript"></script>
<script src="{{url('base-theme/assets/app/js/dashboard.js')}}" type="text/javascript"></script>

<script src="{{URL::asset('frameworks/datatables/jquery.dataTables.min.js')}}"></script>
<!--begin::Base Scripts -->
</body>
<!-- end::Body -->
</html>