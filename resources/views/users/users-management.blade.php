@extends('templates.master')

@section('content')
    <div class="m-content" id="manage-users" xmlns:v-bind="http://www.w3.org/1999/xhtml">
        <div class="row">
            <div class="col-xl-12">
                <!--begin::Portlet-->
                <div class="m-portlet" id="m_portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-bell"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Manage Users
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--pill" onclick="vueApp.addPost()">
                                    <span>
															<i class="la la-plus"></i>
															<span>
																Add Post
															</span>
														</span>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table id="users" class="table table-hover table-condensed" style="width:100%">
                            <thead>
                            <tr>
                                <th width="25%">Username</th>
                                <th width="30%">Email</th>
                                <th width="30%">Stats (Posts | Comments | Reputation)</th>
                                <th width="15%">Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="add_user" role="dialog" aria-labelledby="modalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalLabel">
                            Add Post
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												×
											</span>
                        </button>
                    </div>
                    <form method="POST" action="{{url('users/add-user')}}">
                        {{csrf_field()}}
                        <input name="user_id" :value="user? user.id : null" readonly hidden>
                        <div class="modal-body">
                            <div class="form-group m-form__group row">
                                <div class="col-lg-12">
                                    <label>
                                        Post Name:
                                    </label>
                                    <input type="text" :value="user? user.name : ''" name="user_name" class="form-control m-input"
                                           placeholder="English unique code">
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <div class="col-lg-12">
                                    <label>
                                        Post Summary:
                                    </label>
                                    <textarea rows="3" name="user_summary" type="text" class="form-control m-input"
                                              placeholder="Small English description for user">@{{user? user.summary : '' }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                Close
                            </button>
                            <button type="submit" class="btn btn-primary">
                                Save Post
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="delete_user" role="dialog" aria-labelledby="modalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            Delete Post
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												×
											</span>
                        </button>
                    </div>
                    <form method="user" action="{{url('users/delete-user')}}">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <p>This action will remove a user and it will become unavailable to subscribers. Do you
                                wish to remove?</p>
                        </div>
                        <div class="modal-footer">
                            <input name="user" :value="user ? user.id : null" readonly hidden>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-danger">
                                Delete
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

    <script type="text/javascript">
        // Truncate a string
        function strtrunc(str, max, add){
           add = add || '...';
           return (typeof str === 'string' && str.length > max ? str.substring(0, max) + add : str);
        };

        $(document).ready(function() {
            let oTable = $('#users').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ url('users/get-users') }}",
                "columns": [
                    {data: 'username', name: 'username'},
                    {data: 'email', name: 'email'},
                    {
                        data: null, 
                        orderable: false,
                        render: function (data, type, row) {
                            return row.posts_count + ' | '+ row.comments_count + ' | '+ row.reputation
                        }
                    },
                    {
                        data: null,
                        orderable: false,
                        render: function (data, type, row) {
                            return '<span style="overflow: visible; width: 110px;">\n' +
                                '    <button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn" title="Edit" \n' +
                                '  onclick=\'vueApp.setSelectedEditPost(' + JSON.stringify(row) + ')\'>' +
                                '        <i class="la la-edit"></i>\n' +
                                '    </button>\n' +
                                '    <button  class="delete m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn" title="Delete " \n' +
                                '  onclick=\'vueApp.setSelectedDeletePost(' + JSON.stringify(row) + ')\'>' +
                                '        <i class="la la-trash"></i>\n' +
                                '    </button>\n' +
                                '</span>'
                        }
                    }
                ],
                'columnDefs': [{
                       'targets': [0,1],
                       'render': function(data, type, full, meta){
                          if(type === 'display'){
                             data = strtrunc(data, 150);
                          }
                         
                          return data;
                       }
                    }
                ]
            });
        });

        let vueApp = new Vue({
            el: '#manage-users',
            data: {
                user: null
            },

            methods: {
                setSelectedEditPost: function (data) {
                    this.user = data;
                    $('#add_user').modal('show');
                },
                setSelectedDeletePost: function (data) {
                    this.user = data;
                    $('#delete_user').modal('show');
                },

                addPost: function () {
                    this.user = null;
                    $('#add_user').modal('show');
                },
            }
        });

    </script>

@endsection

