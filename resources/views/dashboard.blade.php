@extends('templates.master')

@section('content')
    <div class="m-content">
        <div class="row">
            <div class="col-xl-12">
                <!--begin::Portlet-->
                <div class="m-portlet" id="m_portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-dashboard"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Dashboard
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">

                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-xl-5 offset-1">
                                <!--begin:: Widgets/Activity-->
                                <div class="m-portlet m-portlet--bordered-semi m-portlet--widget-fit m-portlet--full-height m-portlet--skin-light ">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <h2 class="m-portlet__head-text m--font-light">
                                                    Android App
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-portlet__body">
                                        <div class="m-widget17">
                                            <div class="m-widget17__visual m-widget17__visual--chart m-portlet-fit--top m-portlet-fit--sides m--bg-danger">
                                                <div class="m-widget17__chart" style="height:250px;">
                                                    <h6 class="m-portlet__head-text m--font-light" style="padding-left: 20px;">
                                                        {{$android['title']}} - {{$android['version']}}
                                                    </h6>
                                                    <br/>
                                                    <h6 class="m-portlet__head-text m--font-light" style="padding-left: 20px;">
                                                        {{$android['id']}}
                                                    </h6>
                                                </div>
                                            </div>
                                            <div class="m-widget17__stats">
                                                <div class="m-widget17__items m-widget17__items-col1">
                                                    <div class="m-widget17__item">
														<span class="m-widget17__icon">
															<i class="flaticon-download m--font-brand"></i>
														</span>
                                                        <span class="m-widget17__subtitle">
															{{$android['downloads']}}
														</span>
                                                        <span class="m-widget17__desc">
															Download Range
														</span>
                                                    </div>
                                                    <div class="m-widget17__item">
														<span class="m-widget17__icon">
															<i class="flaticon-line-graph m--font-danger"></i>
														</span>
                                                        <span class="m-widget17__subtitle">
															{{$android['votes']}}
														</span>
                                                        <span class="m-widget17__desc">
															Votes
														</span>
                                                    </div>
                                                </div>
                                                <div class="m-widget17__items m-widget17__items-col2">
                                                    <div class="m-widget17__item">
														<span class="m-widget17__icon">
															<i class="flaticon-diagram m--font-info"></i>
														</span>
                                                        <span class="m-widget17__subtitle">
															{{$android['rating']}}
														</span>
                                                        <span class="m-widget17__desc">
															Application Rating out of 5
														</span>
                                                    </div>
                                                    <div class="m-widget17__item">
														<span class="m-widget17__icon">
															<i class="flaticon-pie-chart m--font-success"></i>
														</span>
                                                        <span class="m-widget17__subtitle">
															{{$android['last_updated']}}
														</span>
                                                        <span class="m-widget17__desc">
															Last Updated
														</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end:: Widgets/Activity-->
                            </div>

                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
@endsection

