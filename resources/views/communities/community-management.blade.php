@extends('templates.master')

@section('content')
    <div class="m-content" id="manage-communities" xmlns:v-bind="http://www.w3.org/1999/xhtml">
        <div class="row">
            <div class="col-xl-12">
                <!--begin::Portlet-->
                <div class="m-portlet" id="m_portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-bell"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Manage Communities
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--pill" onclick="vueApp.addCommunity()">
                                    <span>
															<i class="la la-plus"></i>
															<span>
																Add Community
															</span>
														</span>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table id="communities" class="table table-hover table-condensed" style="width:100%">
                            <thead>
                            <tr>
                                <th width="25%">Community Name</th>
                                <th width="55%">Summary</th>
                                <th width="20%">Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="add_community" role="dialog" aria-labelledby="modalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalLabel">
                            Add Community
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												×
											</span>
                        </button>
                    </div>
                    <form method="POST" action="{{url('communities/add-community')}}">
                        {{csrf_field()}}
                        <input name="community_id" :value="community? community.id : null" readonly hidden>
                        <div class="modal-body">
                            <div class="form-group m-form__group row">
                                <div class="col-lg-12">
                                    <label>
                                        Community Name:
                                    </label>
                                    <input type="text" :value="community? community.name : ''" name="community_name" class="form-control m-input"
                                           placeholder="English unique code">
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <div class="col-lg-12">
                                    <label>
                                        Community Summary:
                                    </label>
                                    <textarea rows="3" name="community_summary" type="text" class="form-control m-input"
                                              placeholder="Small English description for community">@{{community? community.summary : '' }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                Close
                            </button>
                            <button type="submit" class="btn btn-primary">
                                Save Community
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="delete_community" role="dialog" aria-labelledby="modalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            Delete Community
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												×
											</span>
                        </button>
                    </div>
                    <form method="post" action="{{url('communities/delete-community')}}">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <p>This action will remove a community and it will become unavailable to subscribers. Do you
                                wish to remove?</p>
                        </div>
                        <div class="modal-footer">
                            <input name="community" :value="community ? community.id : null" readonly hidden>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-danger">
                                Delete
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

    <script type="text/javascript">
        // Truncate a string
        function strtrunc(str, max, add){
           add = add || '...';
           return (typeof str === 'string' && str.length > max ? str.substring(0, max) + add : str);
        };

        $(document).ready(function() {
            let oTable = $('#communities').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ url('communities/get-communities') }}",
                "columns": [
                    {data: 'name', name: 'name'},
                    {data: 'summary', name: 'summary'},
                    {
                        data: null,
                        orderable: false,
                        render: function (data, type, row) {
                            return '<span style="overflow: visible; width: 110px;">\n' +
                                '    <button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn" title="Edit" \n' +
                                '  onclick=\'vueApp.setSelectedEditCommunity(' + JSON.stringify(row) + ')\'>' +
                                '        <i class="la la-edit"></i>\n' +
                                '    </button>\n' +
                                '    <button  class="delete m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn" title="Delete " \n' +
                                '  onclick=\'vueApp.setSelectedDeleteCommunity(' + JSON.stringify(row) + ')\'>' +
                                '        <i class="la la-trash"></i>\n' +
                                '    </button>\n' +
                                '</span>'
                        }
                    }
                ],
                'columnDefs': [{
                       'targets': 1,
                       'render': function(data, type, full, meta){
                          if(type === 'display'){
                             data = strtrunc(data, 150);
                          }
                         
                          return data;
                       }
                    }
                ]
            });
        });

        let vueApp = new Vue({
            el: '#manage-communities',
            data: {
                community: null
            },

            methods: {
                setSelectedEditCommunity: function (data) {
                    this.community = data;
                    $('#add_community').modal('show');
                },
                setSelectedDeleteCommunity: function (data) {
                    this.community = data;
                    $('#delete_community').modal('show');
                },

                addCommunity: function () {
                    this.community = null;
                    $('#add_community').modal('show');
                },
            }
        });

    </script>

@endsection

