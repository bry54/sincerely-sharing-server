<head>
    <meta charset="utf-8" />
    <title>
        Sincerely Sharing Admin
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Montserrat:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <link href="{{URL::asset('base-theme/assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL::asset('base-theme/assets/demo/demo3/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />

    @if( env('APP_DEBUG') )
        <script src="{{ URL::asset('frameworks/vue/vue.js')}}" type="text/javascript"></script>
    @else
        <script src="{{ URL::asset('frameworks/vue/vue.min.js')}}" type="text/javascript"></script>
    @endif
    <script src="{{ URL::asset('frameworks/axios.min.js')}}" type="text/javascript"></script>

    <link href="{{URL::asset('frameworks/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{URL::asset('frameworks/jquery-3.4.1.min.js')}}"></script>

    <!--end::Base Styles -->
    <link rel="shortcut icon" href="" />
    <style>
        input, textarea, select, td, label{
            color: #000;
            font-size: 15px;
        }
        input[type="text"], textarea[type="text"]{
            font-size:15px;
        }
    </style>
</head>